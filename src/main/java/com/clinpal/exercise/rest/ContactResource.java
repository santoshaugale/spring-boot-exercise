package com.clinpal.exercise.rest;

import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The REST API for the Contact Service
 */
@RestController
@RequestMapping("/api/v1/contacts")
public class ContactResource {

    private static final Logger logger = LogManager.getLogger(ContactResource.class);

    private final ContactService contactService;

    @Autowired
    public ContactResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    /**
     * Add contact API to add valid contact details
     * @param contact
     *        Object of Contact Model class
     * @return ResponseEntity<Contact>
     */
    @ApiOperation(value = "Add Contact API", response = Contact.class)
    @PostMapping
    public ResponseEntity<Contact> addContact(@Validated @RequestBody Contact contact) {
        logger.info("Add contact request : " + contact);
        return ResponseEntity.ok(contactService.addContact(contact));
    }

    /**
     * Rest API to get all available contacts
     * @return ResponseEntity<List<Contact>>
     */
    @ApiOperation(value = "Get all available contacts", response = Contact.class)
    @GetMapping
    public ResponseEntity<List<Contact>> getContacts() {
        logger.info("Get All Contacts");
        return ResponseEntity.ok(contactService.findAll());
    }

    /**
     * Rest API to update contact details
     * @param contact
     *        Object of Contact model
     * @param contactId
     *        Unique id for Contact
     * @return ResponseEntity<Contact>
     */
    @ApiOperation(value = "Update Contact API", response = Contact.class)
    @PutMapping(value = "/{contactId}")
    public ResponseEntity<Contact> updateContact(@Validated @RequestBody Contact contact,
                                                         @PathVariable Long contactId) {
        logger.info("Update contact details for contactId : " + contactId);
        return ResponseEntity.ok(contactService.updateContact(contact, contactId));
    }

    /**
     * Rest API to get contact details by contactId
     * @param contactId
     *        Unique id for Contact
     * @return ResponseEntity<Contact>
     */
    @ApiOperation(value = "API to get contact by Id", response = Contact.class)
    @GetMapping(value = "/{contactId}")
    public ResponseEntity<Contact> getContactById(@PathVariable Long contactId) {
        logger.info("Get contact details for contactId : " + contactId);
        return ResponseEntity.ok(contactService.getContactById(contactId));
    }

    /**
     * Rest API to get contact details by surname
     * @param surname
     *        field from the contact object
     * @return ResponseEntity<List<Contact>>
     */
    @ApiOperation(value = "API to search contact by surname", response = Contact.class)
    @GetMapping(value = "/surname/{surname}")
    public ResponseEntity<List<Contact>> findContactBySurnameLike(@PathVariable String surname) {
        return ResponseEntity.ok(contactService.findContactBySurnameLike(surname));
    }
}
