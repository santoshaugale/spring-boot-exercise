package com.clinpal.exercise.exception;

import com.clinpal.exercise.service.contact.domain.ErrorResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings({"unchecked", "rawtypes"})
/**
 * Custom Exception Handler to handle user defined exceptions
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Exception handler for ContactNotFoundException
     * @param ex
     *          ContactNotFoundException
     * @param request
     * @return ResponseEntity<Object>
     *         Error message along with status code
     */
    @ExceptionHandler(ContactNotFoundException.class)
    public final ResponseEntity<Object> handleContactNotFoundException(ContactNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Contact Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    /**
     * Exception handler for validation errors
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return ResponseEntity<Object>
     *         Error message along with status code
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponse error = new ErrorResponse("Validation Failed", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }
}
