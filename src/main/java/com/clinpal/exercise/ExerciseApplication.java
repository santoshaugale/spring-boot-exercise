package com.clinpal.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point of Spring Boot Application
 */
@SpringBootApplication
public class ExerciseApplication {

	public static void main(String[] args) {
		// Method to bootstrap the Spring Boot Application
		SpringApplication.run(ExerciseApplication.class, args);
	}

}
