package com.clinpal.exercise.service.contact.impl;

import com.clinpal.exercise.exception.ContactNotFoundException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The concrete implementation of the contact service
 */
@Service
class ContactServiceImpl implements ContactService {

    private static final Logger logger = LogManager.getLogger(ContactServiceImpl.class);

    /**
     * The repository DOA layer
     */
    private final ContactRepository repository;

    @Autowired
    private ContactServiceImpl(final ContactRepository repository) {
        this.repository = repository;
    }

    @Override
    public Contact addContact(final Contact contact) {
        logger.info("Inside add contact service");
        return repository.save(contact);
    }

    @Override
    public List<Contact> findAll() {
        List<Contact> allContacts = repository.findAll();
        if (allContacts.isEmpty()) {
            logger.error("No records found in database");
            throw new ContactNotFoundException("No Records Found");
        }
        logger.info(allContacts.size() + "Contacts found");
        return allContacts;
    }

    @Override
    public Contact updateContact(final Contact contact, final Long contactId) {
        Optional<Contact> optionalContact = repository.findById(contactId);

        //Check if Contact is present
        if (optionalContact.isPresent()) {
            logger.info("Updating the contact for contactId : " + contactId);
            Contact existingContact = optionalContact.get();
            contact.setId(contactId);
            //Copy properties of given object into target object using BeanUtils
            BeanUtils.copyProperties(contact, existingContact);
            Contact savedContact = repository.save(existingContact);
            logger.info("Contact updated successfully for contactId : " + contactId);
            return savedContact;
        } else {
            logger.error("Contact not found for contactId : " + contactId);
            throw new ContactNotFoundException("Contact not found for contactId : " + contactId);
        }
    }

    @Override
    public Contact getContactById(final Long contactId) {
        logger.info("Get contact details for contactId : " +contactId);
        Optional<Contact> optionalContact = repository.findById(contactId);
        if (optionalContact.isPresent()) {
            logger.info("Contact found for contactId : " + contactId);
            return optionalContact.get();
        } else {
            logger.error("Contact not found for contactId : " + contactId);
            throw new ContactNotFoundException("Contact details not found for contactId : " + contactId);
        }
    }

    @Override
    public List<Contact> findContactBySurnameLike(final String surname) {
        List<Contact> contactsBySurname = repository.findContactBySurnameLike(surname);
        if (contactsBySurname.isEmpty()) {
            logger.error("No records found for given search criteria");
            throw new ContactNotFoundException("No Records Found");
        }
        logger.info("Contacts found for : " + surname);
        return contactsBySurname;
    }

}
