package com.clinpal.exercise.service.contact;

import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The contact service
 */
public interface ContactService {


    /**
     * Saves a contact to the database.
     *
     * @param contact the contact to save
     * @return the saved contact
     */
    Contact addContact(final Contact contact);

    /**
     * Obtains a list of all contacts.
     *
     * @return Obtains a list of all contacts
     */
    List<Contact> findAll();

    /**
     * Update a contact to the database
     * @param contact Contact details to update
     * @param contactId unique contactId for the Contact to update
     * @return Success response along with status code
     */
    Contact updateContact(final Contact contact, final Long contactId);

    /**
     * Get contact details from database
     * @param contactId unique contactId to get the contact details from DB
     * @return Contact model
     */
    Contact getContactById(final Long contactId);

    /**
     * Obtains a list of contact with a like match to the supplied
     *
     * @param surname
     *         the wildcard surname of contacts to find
     * @return Obtains a list of matching contacts
     */
    List<Contact> findContactBySurnameLike(final String surname);
}
