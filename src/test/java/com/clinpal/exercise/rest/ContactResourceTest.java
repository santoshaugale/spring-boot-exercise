package com.clinpal.exercise.rest;

import com.clinpal.exercise.exception.ContactNotFoundException;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ContactResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ContactService contactService;

    @Test
    void addContactSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();
        Mockito.when(contactService.addContact(contact)).thenReturn(contact);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(post("/api/v1/contacts")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void getContactsSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);

        Mockito.when(contactService.findAll()).thenReturn(contactList);

        mockMvc.perform(get("/api/v1/contacts"))
                .andExpect(status().isOk());
    }

    @Test
    void getContactByIdSuccessTest() throws Exception {

        Contact contact = this.getMockedContact();

        Mockito.when(contactService.getContactById(Mockito.anyLong())).thenReturn(contact);

        mockMvc.perform(get("/api/v1/contacts/1"))
                .andExpect(status().isOk());
    }

    @Test
    void updateContactSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();
        Mockito.when(contactService.updateContact(contact, 1L)).thenReturn(contact);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(put("/api/v1/contacts/1")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void findContactBySurnameLikeTest() throws Exception {
        final String surnameLike = "Kadale";
        Contact contact = this.getMockedContact();
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);
        Mockito.when(contactService.findContactBySurnameLike(Mockito.anyString())).thenReturn(contactList);

        mockMvc.perform(get("/api/v1/contacts/surname/{surname}", surnameLike))
                .andExpect(status().isOk());
    }

    @Test
    void findContactBySurnameLikeRecordsNotFoundTest() throws Exception {
        final String surnameLike = "Kadale";
        Mockito.when(contactService.findContactBySurnameLike(Mockito.anyString())).thenThrow(ContactNotFoundException.class);

        mockMvc.perform(get("/api/v1/contacts/surname/{surname}", surnameLike))
                .andExpect(status().isNotFound());
    }

    @Test
    void addContactValidationFailureTest() throws Exception {
        Contact contact = this.getMockedContact();
        contact.setForename("");
        Mockito.when(contactService.addContact(contact)).thenReturn(contact);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(post("/api/v1/contacts")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getAllContactsRecordsNotFoundTest() throws Exception {
        Mockito.when(contactService.findAll()).thenThrow(ContactNotFoundException.class);

        mockMvc.perform(get("/api/v1/contacts"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getContactByContactIdNotFoundTest() throws Exception {
        Mockito.when(contactService.getContactById(Mockito.anyLong())).thenThrow(ContactNotFoundException.class);

        mockMvc.perform(get("/api/v1/contacts/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateContactFailureTest() throws Exception {
        Contact contact = this.getMockedContact();
        Mockito.when(contactService.updateContact(contact, 1L)).thenThrow(ContactNotFoundException.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(put("/api/v1/contacts/1")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isNotFound());
    }

    private Contact getMockedContact() {
        Contact contact = new Contact();
        contact.setForename("Joseph");
        contact.setSurname("Root");
        contact.setStreet("Dore");
        contact.setCity("SHEFFIELD");
        contact.setPostalCode("S17");
        contact.setCounty("South Yorkshire");
        contact.setCountry("England");
        return contact;
    }
}
