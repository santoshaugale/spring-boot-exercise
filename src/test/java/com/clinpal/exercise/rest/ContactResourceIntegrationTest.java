package com.clinpal.exercise.rest;

import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class ContactResourceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContactService contactService;

    @Test
    void addContactSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(post("/api/v1/contacts")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void getContactsSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();

        contactService.addContact(contact);

        mockMvc.perform(get("/api/v1/contacts"))
                .andExpect(status().isOk());
    }

    @Test
    void findContactBySurnameLikeTest() throws Exception {
        final String surnameLike = "Roo%";
        Contact contact = this.getMockedContact();

        contactService.addContact(contact);

        mockMvc.perform(get("/api/v1/contacts/surname/{surname}", surnameLike))
                .andExpect(status().isOk());
    }

    @Test
    void getContactByIdSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();
        Contact savedContact = contactService.addContact(contact);
        Long contactId = savedContact.getId();

        mockMvc.perform(get("/api/v1/contacts/{contactId}", contactId))
                .andExpect(status().isOk());
    }

    @Test
    void updateContactSuccessTest() throws Exception {
        Contact contact = this.getMockedContact();
        Contact savedContact = contactService.addContact(contact);
        Long contactId = savedContact.getId();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(put("/api/v1/contacts/{contactId}", contactId)
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void addContactValidationFailureTest() throws Exception {
        Contact contact = this.getMockedContact();
        contact.setForename("");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(post("/api/v1/contacts")
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getContactByContactIdNotFoundTest() throws Exception {

        Long contactId = 10L;

        mockMvc.perform(get("/api/v1/contacts/{contactId}", contactId))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateContactFailureTest() throws Exception {
        Contact contact = this.getMockedContact();
        Long contactId = 10L;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(contact);
        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        mockMvc.perform(put("/api/v1/contacts/{contactId}", contactId)
                        .accept(MEDIA_TYPE_JSON_UTF8)
                        .contentType(MEDIA_TYPE_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isNotFound());
    }

    private Contact getMockedContact() {
        Contact contact = new Contact();
        contact.setForename("Joseph");
        contact.setSurname("Root");
        contact.setStreet("Dore");
        contact.setCity("SHEFFIELD");
        contact.setPostalCode("S17");
        contact.setCounty("South Yorkshire");
        contact.setCountry("England");
        return contact;
    }

}
