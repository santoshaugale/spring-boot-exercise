package com.clinpal.exercise.service.contact.domain;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ErrorResponseTest {

    private static final LocalDateTime timestamp = LocalDateTime.now();
    private static final String message = "Contact Not Found";
    private static final List<String> details = Arrays.asList("foreName can not be empty");
    private static final Integer statusCode = 404;
    ErrorResponse errorResponse = new ErrorResponse();

    @Test
    void setTimestamp() {
        errorResponse.setTimestamp(timestamp);
        assertThat(errorResponse.getTimestamp(), is(timestamp));
    }

    @Test
    void setMessage() {
        errorResponse.setMessage(message);
        assertThat(errorResponse.getMessage(), is(message));
    }

    @Test
    void setDetails() {
        errorResponse.setDetails(details);
        assertThat(errorResponse.getDetails(), is(details));
    }
}

