package com.clinpal.exercise.service.contact.impl;

import com.clinpal.exercise.exception.ContactNotFoundException;
import com.clinpal.exercise.service.contact.domain.Contact;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ContactServiceImplTest {

    @Spy
    @InjectMocks
    private ContactServiceImpl contactService;

    @Mock
    private ContactRepository repository;

    @Test
    void addContactSuccessTest() {
        Contact mockedContact = this.getMockedContact();
        Mockito.when(repository.save(mockedContact)).thenReturn(mockedContact);

        Contact contact = contactService.addContact(mockedContact);
        assertEquals(mockedContact, contact);
    }

    @Test
    void getAllContactsSuccessTest() {
        Contact contact = this.getMockedContact();
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);

        Mockito.when(repository.findAll()).thenReturn(contactList);

        List<Contact> expectedContactList = contactService.findAll();
        assertEquals(contactList, expectedContactList);
    }

    @Test
    void findContactBySurnameLikeSuccessTest() {
        Contact contact = this.getMockedContact();
        List<Contact> contactList = new ArrayList<>();
        contactList.add(contact);

        Mockito.when(repository.findContactBySurnameLike(Mockito.anyString())).thenReturn(contactList);

        List<Contact> expectedContactList = contactService.findContactBySurnameLike(Mockito.anyString());
        assertEquals(contactList, expectedContactList);
    }
    @Test
    void findContactBySurnameLikeNotFoundTest() {

        Assertions.assertThrows(ContactNotFoundException.class, () -> {
            List<Contact> mockedContactList = new ArrayList<>();
            Mockito.when(repository.findContactBySurnameLike(Mockito.anyString())).thenReturn(mockedContactList);
            List<Contact> expectedContactList = contactService.findContactBySurnameLike(Mockito.anyString());
        });
    }

    @Test
    void getContactByIdSuccessTest() {
        Optional<Contact> mockedContact = Optional.of(this.getMockedContact());
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(mockedContact);
        Contact contactResponse = contactService.getContactById(Mockito.anyLong());
        assertEquals(mockedContact.get(), contactResponse);
    }

    @Test
    void updateUserByIdSuccessTest() {
        Contact mockedContact = this.getMockedContact();
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.of(mockedContact));
        Mockito.when(repository.save(mockedContact)).thenReturn(mockedContact);
        Contact userModel = contactService.updateContact(mockedContact, 1l);
        assertNotNull(userModel);
    }

    @Test
    void getAllContactsFailureTest() {

        Assertions.assertThrows(ContactNotFoundException.class, () -> {
            List<Contact> mockedContactList = new ArrayList<>();
            Mockito.when(repository.findAll()).thenReturn(mockedContactList);
            List<Contact> expectedContactList = contactService.findAll();
        });
    }

    @Test
    void getContactByIdContactNotFoundExceptionTest() {

        Assertions.assertThrows(ContactNotFoundException.class, () -> {
            Optional<Contact> mockedContact = Optional.empty();
            Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(mockedContact);
            Mockito.when(contactService.getContactById(Mockito.anyLong())).thenReturn(mockedContact.get());
        });
    }

    @Test
    void updateContactByIdContactNotFoundExceptionTest() {
        Contact contact = this.getMockedContact();
        Assertions.assertThrows(ContactNotFoundException.class, () -> {
            Optional<Contact> mockedContact = Optional.empty();
            Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(mockedContact);
            Mockito.when(contactService.updateContact(contact, 1l)).thenReturn(contact);
        });
    }

    private Contact getMockedContact() {
        Contact contact = new Contact();
        contact.setForename("Joseph");
        contact.setSurname("Root");
        contact.setStreet("Dore");
        contact.setCity("SHEFFIELD");
        contact.setPostalCode("S17");
        contact.setCounty("South Yorkshire");
        contact.setCountry("England");
        return contact;
    }

}
