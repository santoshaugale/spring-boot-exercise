package com.clinpal.exercise;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.clinpal.exercise.rest.ContactResource;
import com.clinpal.exercise.service.contact.impl.ContactRepository;

@SpringBootTest
class ExerciseApplicationTests {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactResource contactResource;


    @Test
    void contextLoads() {
        assertThat(contactRepository, is(notNullValue()));
        assertThat(contactResource, is(notNullValue()));
    }

    @Test
    public void applicationStarts() {
        ExerciseApplication.main(new String[] {});
    }
}
